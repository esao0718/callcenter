# Ejercicio de Java

El objetivo de este ejercicio es conocer cómo los candidatos a entrar a
almundo.com usan herramientas básicas de Java y diseñan soluciones
orientadas a objetos.


**Se creo un artefacto basado en springboot con metodos REST para simular las llamadas y listar su estado ya sea CANCELADAS o ATAENDIDAS la clase que levanta la aplicación es CallCenterApplication.java **


**Para ejecutar mvn package && java -jar target/callcenterjmora-0.0.1-SNAPSHOT.jar **


** http://localhost:8080/callcenter/llamar  Para simular llamadas **


** http://localhost:8080/callcenter/llamadas  Para ver el listado de llamadas y sus estados **



# Forma de entrega

Se entrega link de repositorio GITLAB:  https://gitlab.com/esao0718/callcenter.git



# Consigna (Respuestas)

Existe un call center donde hay 3 tipos de empleados: operador, supervisor

y director.

**Se creó una clase Empleado tipificando ordinalmente con una enumeración OPERADOR, SUPERVISOR y DIRECTOR (TipoEmpleado)**

El proceso de la atención de una llamada telefónica en primera

instancia debe ser atendida por un operador, si no hay ninguno libre debe

ser atendida por un supervisor, y de no haber tampoco supervisores libres

debe ser atendida por un director.

**Se creó la clase ControlTurno el cual valida esta regla**

**Requerimientos**

Debe existir una clase Dispatcher encargada de manejar las

llamadas, y debe contener el método dispatchCall para que las

asigne a los empleados disponibles.

**src/main/java/com/almundo/callcenterjmora/data/jpa/web/Dispatcher.java**

** **

El método dispatchCall puede invocarse por varios hilos al mismo

tiempo.

** **

**El método dispatchCall de la clase Dispacher envía la llamada a la clase ControlLlamada la cual instancia un objeto Callable el cual es entregado al pool de hilos para simular la atención por línea.**

La clase Dispatcher debe tener la capacidad de poder procesar 10

llamadas al mismo tiempo (de modo concurrente).

**La configuración del Pool de hilos en ControlLlamada está dada por  Executors.newFixedThreadPool(10) el cual me garantiza la atención siempre en paralelo y simula la espera, cuando todas las lineas (hilos) están ocupados.**



Cada llamada puede durar un tiempo aleatorio entre 5 y 10

segundos.

**Dentro de la Clase AtencionLlamadaCallable se tiene la siguiente lógica para simular la espera cuando una instancia esté en ejecución por el pool:**

** **

**Long tiempoLlamada = new Double(Math.floor(Math.random() \* (10000 - 5000 + 1) + 5000)).longValue();**

**Thread.sleep(tiempoLlamada);**



Debe tener un test unitario donde lleguen 10 llamadas.

** **

**Se encuentra dentro del paquete de src/test/\* se encuentra la simulación por medio de 10 peticiones al REST**
**/callcenter/llamar**

**Tambien se agrego un metrodo REST para visualizar la lista de llamadas ya sea atendidas o canceladas**
**/callcenter/llamadas**


** **









**Extras/Plus**

Dar alguna solución sobre qué pasa con una llamada cuando no hay

ningún empleado libre.

**Cuando no se encuentra ningún empleado libre se creó la entidad llamada la cual se registra en una DB en memoria  este caso, tipificando con la enumeración EstadoLlamada  CANCELADA para cuando se presente ese  caso y ATENDIDA, para este último caso se persiste el empleado el tiempo que duro y cuando (datetime) atendió el operador.**

** **

Dar alguna solución sobre qué pasa con una llamada cuando entran

más de 10 llamadas concurrentes.

La clase ControlLlamda instancia un pool de hilos 10, los cuales me garantizan que se procese siempre en paralelo lo que se envíe, si todos los hilos se encuentran ocupados (líneas)