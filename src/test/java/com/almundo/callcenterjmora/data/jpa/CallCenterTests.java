package com.almundo.callcenterjmora.data.jpa;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.almundo.callcenterjmora.data.jpa.service.EmpleadoService;
import com.almundo.callcenterjmora.data.jpa.web.ControlLlamadas;
import com.almundo.callcenterjmora.data.jpa.web.ControlTurno;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CallCenterApplication.class)
@WebAppConfiguration
@ActiveProfiles("scratch")

public class CallCenterTests {

	@Autowired
	private WebApplicationContext context;

	private MockMvc mvc;

	@Before
	public void setUp() {
		this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
		ControlLlamadas.getInstance().start();
		EmpleadoService empleadoService = (EmpleadoService) context.getBean("empleadoService");
		ControlTurno.getInstance().setListEmpleados(empleadoService.findAll());
	}

	@Test
	public void estaOk() throws Exception {

		this.mvc.perform(get("/callcenter")).andExpect(status().isOk()).andExpect(content().string("OK"));
	}

	/**
	 * Requerimientos Debe tener un test unitario donde lleguen 10 llamadas.
	 * 
	 * @throws Exception
	 */
	@Test
	public void llamar10() throws Exception {

		for (int i = 0; i < 10; i++) {
			this.mvc.perform(get("/callcenter/llamar")).andExpect(status().isOk())
					.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
		}

	}

	@Test
	public void llamadas() throws Exception {

		this.mvc.perform(get("/callcenter/llamadas").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());

	}

}
