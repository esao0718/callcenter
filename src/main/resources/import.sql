


-- Operadores

insert into empleado(nombre, tipo_empleado) values ('Mario Andres Martinez', 0)
insert into empleado(nombre, tipo_empleado) values ('Jorge Eliecer Martinez', 0)
insert into empleado(nombre, tipo_empleado) values ('Ubaldo Jose Elles', 0)
insert into empleado(nombre, tipo_empleado) values ('Doris Herrera', 0)
insert into empleado(nombre, tipo_empleado) values ('Roberto Carlos Montiel', 0)
insert into empleado(nombre, tipo_empleado) values ('Leonor del Socorro', 0)
insert into empleado(nombre, tipo_empleado) values ('Sixta Tulia Rojas', 0)
insert into empleado(nombre, tipo_empleado) values ('Ubaldo Jose Herrera', 0)
insert into empleado(nombre, tipo_empleado) values ('Victor Andres Belford', 0)
insert into empleado(nombre, tipo_empleado) values ('Harol David Belford', 0)


-- Supervisores
insert into empleado(nombre, tipo_empleado) values ('Andes David  Orozco', 1)
insert into empleado(nombre, tipo_empleado) values ('Milena Tatiana', 1)
insert into empleado(nombre, tipo_empleado) values ('Beatriz Villadiego', 1)

-- Directores
insert into empleado(nombre, tipo_empleado) values ('Nathalí Elles Herrera', 2)
insert into empleado(nombre, tipo_empleado) values ('José Luis Mora Villadiego', 2)

