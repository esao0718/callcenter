package com.almundo.callcenterjmora.data.jpa.web;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.almundo.callcenterjmora.data.jpa.domain.Empleado;
import com.almundo.callcenterjmora.data.jpa.domain.EstadoLlamada;
import com.almundo.callcenterjmora.data.jpa.domain.Llamada;
import com.almundo.callcenterjmora.data.jpa.service.LlamadaService;

/**
 * Requerimientos: Debe existir una clase Dispatcher encargada de manejar las
 * llamadas, y debe contener el método dispatchCall para que las asigne a los
 * empleados disponibles.
 * 
 * 
 * 
 * Encargado obtener {@link Empleado} para atender {@link Llamada} o cancelarla
 * si no existe uno disponible
 * 
 * @author jmora
 *
 */
@Component
public class Dispatcher {

	private final LlamadaService llamadaService;



	@Autowired
	public Dispatcher(LlamadaService llamadaService) {
		this.llamadaService = llamadaService;
	}

	/**
	 * El método dispatchCall instancia un objeto {@link AtencionLlamadaCallable}
	 * para ser enviado {@link ControlLlamadas} y ser atendido
	 * 
	 * @param llamada
	 * @return {@link Llamada}
	 */
	public Llamada dispatchCall(Llamada llamada) {

		Optional<Empleado> empleadoAtencion = ControlTurno.getInstance().getEmpleadoTurno();

		if (empleadoAtencion.isPresent()) {

			/**
			 * Requerimientos El método dispatchCall puede invocarse por varios hilos al
			 * mismo tiempo. Esto se resuelve con el envio al control de llamadas por el
			 * objeto {@link ExecutorService#newFixedThreadPool} enviandole como parametro
			 * 10 el cual es el numero de hilos en paralelo para atender las llamadas
			 */
			AtencionLlamadaCallable atencionLlamada = new AtencionLlamadaCallable(empleadoAtencion.get(), llamada,
					llamadaService);
			ControlLlamadas.getInstance().atenderLlamda(atencionLlamada);
		} else {
			
			
			llamada.setFechaHora(new Date());
			llamada.setEstdoLlamada(EstadoLlamada.CANCELADA);
			llamada.setObservacion("Llamada CANCELADA por falta de Empleados para atender!");
			llamadaService.save(llamada);
		}

		return llamada;
	}

}
