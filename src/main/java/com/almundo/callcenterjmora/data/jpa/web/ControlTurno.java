package com.almundo.callcenterjmora.data.jpa.web;

import java.util.List;
import java.util.Optional;

import com.almundo.callcenterjmora.data.jpa.domain.Empleado;
import com.almundo.callcenterjmora.data.jpa.domain.TipoEmpleado;

/**
 * Control de turnos para empleados para atender llamadas
 * 
 * @author jmora
 *
 */
public class ControlTurno {

	private static ControlTurno controlTurno;

	private List<Empleado> listEmpleados;

	private ControlTurno() {

	}

	public static synchronized ControlTurno getInstance() {
		if (controlTurno == null) {
			controlTurno = new ControlTurno();
		}
		return controlTurno;
	}

	public void setListEmpleados(List<Empleado> listEmpleados) {
		this.listEmpleados = listEmpleados;
	}

	/**
	 * Devuelve en primera instancia un {@link Empleado} de tipo
	 * {@link TipoEmpleado#OPERADOR} en primera instancia, en segunda un
	 * {@link TipoEmpleado#SUPERVISOR}n y por ultimo un
	 * {@link TipoEmpleado#DIRECTOR}
	 * 
	 * @return
	 */
	public Optional<Empleado> getEmpleadoTurno() {

		Optional<Empleado> empleado = null;
		empleado = listEmpleados.stream().filter(e -> TipoEmpleado.OPERADOR.equals(e.getTipoEmpleado())).findFirst();
		if (!empleado.isPresent()) {
			empleado = listEmpleados.stream().filter(e -> TipoEmpleado.SUPERVISOR.equals(e.getTipoEmpleado()))
					.findFirst();
		}
		if (!empleado.isPresent()) {
			empleado = listEmpleados.stream().filter(e -> TipoEmpleado.DIRECTOR.equals(e.getTipoEmpleado()))
					.findFirst();
		}

		if (empleado.isPresent()) {
			listEmpleados.remove(empleado.get());
		}

		return empleado;
	}

	public void desocuparEmpleado(Empleado empleado) {
		listEmpleados.add(empleado);

	}

}
