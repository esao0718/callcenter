package com.almundo.callcenterjmora.data.jpa.service;

import java.util.List;

import com.almundo.callcenterjmora.data.jpa.domain.Llamada;

public interface LlamadaService {
	public Llamada save(Llamada save);

	public List<Llamada> findAll();
}
