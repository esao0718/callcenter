package com.almundo.callcenterjmora.data.jpa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.almundo.callcenterjmora.data.jpa.domain.Llamada;

@Service("llamadaService")
@Transactional
public class LlamadaServiceImpl implements LlamadaService {

	private final LlamadaRepository llamadaRepository;

	@Autowired
	public LlamadaServiceImpl(LlamadaRepository llamadaRepository) {
		this.llamadaRepository = llamadaRepository;
	}

	@Override
	public Llamada save(Llamada save) {
		return llamadaRepository.save(save);

	}

	@Override
	public List<Llamada> findAll() {
		return llamadaRepository.findAll();
	}

}
