package com.almundo.callcenterjmora.data.jpa.web;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Requerimiento La clase Dispatcher debe tener la capacidad de poder procesar
 * 10 llamadas al mismo tiempo (de modo concurrente).
 * 
 * 
 * Control de llamdas, con el minimo de atencion en paralelo de 10 llamdas sobre
 * el objeto {@link ExecutorService}
 * 
 * @author jmora
 *
 */
public class ControlLlamadas {

	private ExecutorService executorService;
	private static ControlLlamadas controlLlamadas;
	private List<AtencionLlamadaCallable> atencionLlamadaCallables;

	public static synchronized ControlLlamadas getInstance() {
		if (controlLlamadas == null) {
			controlLlamadas = new ControlLlamadas();
		}
		return controlLlamadas;

	}

	private ControlLlamadas() {
		atencionLlamadaCallables = new ArrayList<>();
	}

	public void atenderLlamda(AtencionLlamadaCallable atencionLlamadaCallable) {

		executorService.submit(atencionLlamadaCallable);
		atencionLlamadaCallables.add(atencionLlamadaCallable);
	}

	public void start() {
		/*
		 * 10 LLamadas minimo siempre en paralelo, procesa la siguiente cuando un hilo
		 * se desocupe
		 */
		executorService = Executors.newFixedThreadPool(10);

	}

}
