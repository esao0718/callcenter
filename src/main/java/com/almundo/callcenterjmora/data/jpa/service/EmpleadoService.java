package com.almundo.callcenterjmora.data.jpa.service;


import java.util.List;

import com.almundo.callcenterjmora.data.jpa.domain.Empleado;

public interface EmpleadoService {

	List<Empleado> findAll();

}
