package com.almundo.callcenterjmora.data.jpa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.almundo.callcenterjmora.data.jpa.domain.Empleado;

@Service("empleadoService")
@Transactional
public class EmpleadoServiceImpl implements EmpleadoService {

	private final EmpleadoRepository empleadoRepository;

	@Autowired
	public EmpleadoServiceImpl(EmpleadoRepository empleadoRepository) {
		this.empleadoRepository = empleadoRepository;
	}

	@Override
	public List<Empleado> findAll() {
		return empleadoRepository.findAll();
	}

}
