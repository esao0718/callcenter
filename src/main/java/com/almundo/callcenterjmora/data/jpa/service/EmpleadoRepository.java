package com.almundo.callcenterjmora.data.jpa.service;


import java.util.List;

import org.springframework.data.repository.Repository;

import com.almundo.callcenterjmora.data.jpa.domain.Empleado;



public interface EmpleadoRepository extends Repository<Empleado,Long> {
	
	List<Empleado> findAll();

}
