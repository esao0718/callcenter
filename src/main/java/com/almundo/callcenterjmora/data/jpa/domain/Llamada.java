package com.almundo.callcenterjmora.data.jpa.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Llamada implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2969644081211136458L;

	@Id
	@GeneratedValue
	private Long id;

	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss a")
	private Date fechaHora;

	@Column
	private Long tiempoAtencion;

	@Column(nullable = false)
	@Enumerated(EnumType.ORDINAL)
	private EstadoLlamada estdoLlamada;

	@ManyToOne
	private Empleado empleadoAtencion;

	@Column(nullable = false)
	private String observacion;

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}

	public Long getTiempoAtencion() {
		return tiempoAtencion;
	}

	public void setTiempoAtencion(Long tiempoAtencion) {
		this.tiempoAtencion = tiempoAtencion;
	}

	public EstadoLlamada getEstdoLlamada() {
		return estdoLlamada;
	}

	public void setEstdoLlamada(EstadoLlamada estdoLlamada) {
		this.estdoLlamada = estdoLlamada;
	}

	public Empleado getEmpleadoAtencion() {
		return empleadoAtencion;
	}

	public void setEmpleadoAtencion(Empleado empleadoAtencion) {
		this.empleadoAtencion = empleadoAtencion;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Llamada [id=");
		builder.append(id);
		builder.append(", fechaHora=");
		builder.append(fechaHora);

		builder.append(", tiempoAtencion=");
		builder.append(tiempoAtencion);
		builder.append(", estdoLlamada=");
		builder.append(estdoLlamada);
		builder.append(", empleadoAtencion=");
		builder.append(empleadoAtencion);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((empleadoAtencion == null) ? 0 : empleadoAtencion.hashCode());
		result = prime * result + ((estdoLlamada == null) ? 0 : estdoLlamada.hashCode());

		result = prime * result + ((fechaHora == null) ? 0 : fechaHora.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((observacion == null) ? 0 : observacion.hashCode());
		result = prime * result + ((tiempoAtencion == null) ? 0 : tiempoAtencion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Llamada other = (Llamada) obj;
		if (empleadoAtencion == null) {
			if (other.empleadoAtencion != null) {
				return false;
			}
		} else if (!empleadoAtencion.equals(other.empleadoAtencion)) {
			return false;
		}
		if (estdoLlamada != other.estdoLlamada) {
			return false;
		}

		if (fechaHora == null) {
			if (other.fechaHora != null) {
				return false;
			}
		} else if (!fechaHora.equals(other.fechaHora)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (observacion == null) {
			if (other.observacion != null) {
				return false;
			}
		} else if (!observacion.equals(other.observacion)) {
			return false;
		}
		if (tiempoAtencion == null) {
			if (other.tiempoAtencion != null) {
				return false;
			}
		} else if (!tiempoAtencion.equals(other.tiempoAtencion)) {
			return false;
		}
		return true;
	}

}
