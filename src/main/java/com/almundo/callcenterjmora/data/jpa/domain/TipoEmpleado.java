package com.almundo.callcenterjmora.data.jpa.domain;


public enum TipoEmpleado {

	OPERADOR, SUPERVISOR, DIRECTOR;

}
