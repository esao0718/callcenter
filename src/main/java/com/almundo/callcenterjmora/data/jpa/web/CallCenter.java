/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.almundo.callcenterjmora.data.jpa.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.almundo.callcenterjmora.data.jpa.domain.Llamada;
import com.almundo.callcenterjmora.data.jpa.service.LlamadaService;

@RestController
@RequestMapping("/callcenter")
public class CallCenter {

	private final Dispatcher dispatcher;
	private final LlamadaService llamadaService;

	@Autowired
	public CallCenter(Dispatcher dispatcher, LlamadaService llamadaService) {
		this.dispatcher = dispatcher;
		this.llamadaService = llamadaService;
	}

	@RequestMapping
	@ResponseBody
	public String ok() {

		return "OK";
	}

	@RequestMapping("/llamar")
	@ResponseBody
	public Llamada llamar() {

		Llamada llamada = new Llamada();
		

		return dispatcher.dispatchCall(llamada);
	}

	@RequestMapping("/llamadas")
	@ResponseBody
	public List<Llamada> llamadas() {
		return llamadaService.findAll();
	}

}
