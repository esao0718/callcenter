package com.almundo.callcenterjmora.data.jpa.service;

import java.util.List;

import org.springframework.data.repository.Repository;

import com.almundo.callcenterjmora.data.jpa.domain.Llamada;

public interface LlamadaRepository extends Repository<Llamada, Long> {

	Llamada save(Llamada save);

	List<Llamada> findAll();

}
