package com.almundo.callcenterjmora.data.jpa.web;

import java.util.Date;
import java.util.concurrent.Callable;

import com.almundo.callcenterjmora.data.jpa.domain.Empleado;
import com.almundo.callcenterjmora.data.jpa.domain.EstadoLlamada;
import com.almundo.callcenterjmora.data.jpa.domain.Llamada;
import com.almundo.callcenterjmora.data.jpa.service.LlamadaService;

/**
 * 
 * @author jmora
 *
 */

public class AtencionLlamadaCallable implements Callable<Void> {

	private LlamadaService llamadaService;

	private Llamada llamada;

	private Empleado empleado;

	public AtencionLlamadaCallable(Empleado empleado, Llamada llamada, LlamadaService llamadaService) {
		this.llamada = llamada;
		this.llamadaService = llamadaService;
		this.empleado = empleado;
	}

	@Override
	public Void call() throws Exception {

		try {

			/**
			 * Requerimineto Cada llamada puede durar un tiempo aleatorio entre 5 y 10
			 * segundos.
			 */

			Long tiempoLlamada = new Double(Math.floor(Math.random() * (10000 - 5000 + 1) + 5000)).longValue();
			Thread.sleep(tiempoLlamada);
			llamada.setEstdoLlamada(EstadoLlamada.ATENDIDA);
			llamada.setFechaHora(new Date());
			llamada.setTiempoAtencion(new Long(tiempoLlamada / 1000));
			llamada.setEmpleadoAtencion(empleado);
			llamada.setObservacion("LLamada ATENDIDA");
			llamadaService.save(llamada);
			ControlTurno.getInstance().desocuparEmpleado(empleado);

		} catch (Exception e) {
			e.printStackTrace(); // Cambiar por Log
		}
		return null;
	}

}
